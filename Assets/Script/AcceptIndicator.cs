using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicFight
{
    public class AcceptIndicator : MonoBehaviour
    {

        [SerializeField] private Renderer _renderer;

        public itemColor color { get; private set; }
        // Start is called before the first frame update

        private void SetColor(Color color)
        {
            _renderer.material.color = color;
        }

        public void SetColor(itemColor color)
        {
            this.color = color;
            SetColor(ColorManager.main.colors[(int)color]);
        }
    }
}
