using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MagicFight
{
    public class LogicAtack : MonoBehaviour
    {
    
        [SerializeField] private int HPMob = 5;
        private int HPlayer = 5;
        private int HPMobMax = 5;
        [SerializeField] private GameObject[] _prefab;
        private GameObject _obj;
        [SerializeField] private Image healthBoss;
        [SerializeField] private Text healthBossText;
        


        void Start()
        {
            _obj =Instantiate(_prefab[Random.Range(0,_prefab.Length-1)], transform.position, Quaternion.identity, transform);
            healthBoss.fillAmount = 1;
            healthBossText.text = HPMobMax.ToString();
        }

        public void GetDamage()
        {
            HPMob -= 1;
            healthBoss.fillAmount = (float)HPMob / HPMobMax;
            healthBossText.text = HPMob.ToString();
            if (HPMob <= 0)
            {Destroy(_obj);
                _obj = Instantiate(_prefab[Random.Range(0, _prefab.Length - 1)], transform.position, Quaternion.identity,
                    transform);
                HPMobMax += 2;
                HPMob = HPMobMax;
            }
        }
        
    }
}
