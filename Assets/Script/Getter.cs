using System;
using System.Collections.Generic;
using MagicFight;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Getter : MonoBehaviour
{
 
    private DragItem _item;
    private ActionEnum selectedAction = ActionEnum.None;

    [SerializeField] private AcceptIndicator[] indicators;
    [SerializeField]private LogicAtack _logicAtack;
    [SerializeField] private Image comboCount;

    private int ComboCount = 0;
    private int ComboCountMax = 3;

    private void Start()
    {
        for (int i = 0; i < indicators.Length; i++)
        {
            indicators[i].SetColor(GetRandonColor());
        }
    }

    public itemColor GetRandonColor()
    {
        var colors = new List<itemColor>(ColorManager.main.colorNames);

        for (int i = 0; i < indicators.Length; i++)
        {
            colors.Remove(indicators[i].color);
        }
        return colors[Random.Range(0, colors.Count-1)];
    }

    /// <summary>
    /// Предмет в тригере
    /// </summary>
    private void OnTriggerStay(Collider other)
    {
        var item = other.GetComponent<DragItem>();
        if (item == null || item.isDragable)
        {
            _item = item;
            return;
        }
        if (!item.isDragable && _item == item)
        {
            TryGetItem();
            _item = null;
            return;
        }
    }

    // GOTO проверка то что выбран элемент из нужного списка, уничтожение объекта с генерациией магии или получение дамага
    /// <summary>
    /// Проверка корректности выбора
    /// </summary>
    private void TryGetItem()
    {
        for (int i = 0; i < indicators.Length; i++)
        {
            if (indicators[i].color == _item.Color)
            {
                if (selectedAction == ActionEnum.None)
                {
                    selectedAction = (ActionEnum)i;
                    GetItem(i);
                    return;
                }
                if (selectedAction == (ActionEnum)i)
                {
                    GetItem(i);
                    return;
                }
            }
        }
        
        comboCount.fillAmount = 0;
        selectedAction = ActionEnum.None;
        ComboCount = 0;
        Destroy(_item.gameObject);
    }

    private void GetItem(int indicatorIndex)
    {
        indicators[indicatorIndex].SetColor(GetRandonColor());
        ComboCount++;
        comboCount.fillAmount += 1.0f / ComboCountMax;
        
        if (ComboCount >= ComboCountMax)
        {
            comboCount.fillAmount = 0;
            ComboCount = 0;
            _logicAtack.GetDamage();
            return;
        }
        Destroy(_item.gameObject);
    }
}