using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicFight
{
    public class spawner : MonoBehaviour
    {
        [SerializeField] private int _count;
        [SerializeField] private GameObject _prefab;
        [SerializeField] private Vector3 _range;
        
        

        // Start is called before the first frame update
        void Start()
        {
            for (int i = 0; i < _count; i++)
            {
                Vector3 ofset = new Vector3(
                    Random.Range(-_range.x, _range.x), 
                    Random.Range(-_range.y, _range.y), 
                    Random.Range(-_range.z, _range.z));
                var obj =Instantiate(_prefab, transform.position + ofset, Quaternion.identity, transform);
                obj.transform.parent = transform;
            }
        }
        
        void rewpawn()
        {
            Vector3 ofset = new Vector3(
                Random.Range(-_range.x, _range.x), 
                Random.Range(-_range.y, _range.y), 
                Random.Range(-_range.z, _range.z));
            var obj =Instantiate(_prefab, transform.position + ofset, Quaternion.identity, transform);
            obj.transform.parent = transform;
        }
        
    }
}
