using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MagicFight
{
    public class DragItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] private float upForse = 30;
        private Rigidbody _ridigbody;
        [SerializeField]private itemColor color;

        public itemColor Color => color;


        public bool isDragable { get; private set; }

        private void Start()
        {
            _ridigbody = GetComponent<Rigidbody>();
        }

        /// <summary>
        /// палец отпущен
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            _ridigbody.isKinematic = true;
            isDragable = true;
        }

        /// <summary>
        /// палец нажат
        /// </summary>
        public void OnPointerUp(PointerEventData eventData)
        {
            _ridigbody.isKinematic = false;
            _ridigbody.AddForce(Vector3.up * upForse);
            isDragable = false;
        }

        /// <summary>
        /// перетаскивание объекта
        /// </summary>
        public void OnDrag(PointerEventData eventData)
        {
            var pos = eventData.pointerCurrentRaycast.worldPosition;
            pos.y = 2;
            var delta = pos - transform.position;
            delta.y = 0;

            transform.position += delta;
        }
    }
}