using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicFight
{
    public class ColorManager : MonoBehaviour
    {
        public static ColorManager main;
        public itemColor[] colorNames;

        public Color[] colors; 
        // Start is called before the first frame update
        private void Awake()
        {
            main = this;
            colorNames = (itemColor[])Enum.GetValues(typeof(itemColor));
        }
        
    }
}
